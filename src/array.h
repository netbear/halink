#ifndef ARRAY_H
#define ARRAY_H

#include "typetraits.h"

template <class T, int CAPACITY> class Array
{
public:
    typedef T value_t;
    typedef typename integer_type<CAPACITY>::type size_t;

    value_t &operator[](size_t index)
    {
        return data_[index];
    }

    const value_t &operator[](size_t index) const
    {
        return data_[index];
    }

    size_t size() const
    {
        return CAPACITY;
    }

private:
    value_t data_[CAPACITY];
};

#endif // ARRAY_H
