#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

#include <inttypes.h>

template <typename T, uint16_t CAPACITY, bool OVERWRITE = true>
class CircularBuffer
{
    using pos_t = uint8_t;

public:
    CircularBuffer() : head_(0), tail_(0)
    {
    }

    bool push(T value)
    {
        pos_t headNext = next_(head_);

        // Check for overflow
        if(headNext == tail_)
        {
            if constexpr(OVERWRITE)
            {
                tail_ = next_(tail_);
            }
            else
            {
                return false;
            }
        }

        // Insert value
        data_[head_] = value;
        head_ = headNext;

        return true;
    }

    // Only move to the next item, leave the memory slot intact.
    bool push()
    {
        pos_t headNext = next_(head_);

        // Check for overflow
        if(headNext == tail_)
        {
            if constexpr(OVERWRITE)
            {
                tail_ = next_(tail_);
            }
            else
            {
                return false;
            }
        }

        head_ = headNext;
    }

    T pop()
    {
        T c = data_[tail_];
        tail_ = next_(tail_);
        return c;
    }

    T &peek(pos_t index = 0)
    {
        return data_[next_(tail_, index)];
    }

    const T &peek(pos_t index = 0) const
    {
        return data_[next_(tail_, index)];
    }

    const T &peekHead(pos_t index = 0) const
    {
        return data_[prev_(head_, index + 1)];
    }

    T &peekHead(pos_t index = 0)
    {
        return data_[prev_(head_, index + 1)];
    }

    void clear()
    {
        tail_ = head_;
    }

    bool full() const
    {
        return next_(head_) == tail_;
    }

    bool empty() const
    {
        return head_ == tail_;
    }

    uint16_t size() const
    {
        return (CAPACITY + head_ - tail_) % CAPACITY;
    }

    uint16_t capacity() const
    {
        return CAPACITY;
    }

private:
    pos_t next_(pos_t current, pos_t increment = 1) const
    {
        return (current + increment) % CAPACITY;
    }

    pos_t prev_(pos_t current, pos_t decrement = 1) const
    {
        return (current - decrement) % CAPACITY;
    }

    volatile pos_t head_;
    volatile pos_t tail_;
    T data_[CAPACITY];
};

#endif
