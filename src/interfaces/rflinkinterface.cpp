#include "rflinkinterface.h"

#include "../stringtokenizer.h"
#include "../utils.h"

RfLinkInterface::RfLinkInterface() : nextIndex_(1)
{
    uart0.init(uartBaudSelect(57600));
    sendVersion_();
}

void RfLinkInterface::send(const RfSignal &signal)
{
    sendHeader_();

    send_(signal.size(), 10);
    uart0.write(';');

    for(int i = 0; i < signal.size(); ++i)
    {
        send_(signal.peek(i), 10);
        uart0.write(';');
    }

    uart0.write("\r\n");
}

void RfLinkInterface::send(const RfMessage &message)
{
    sendHeader_();

    // Send protocol
    switch(message.protocol())
    {
        case Protocol::NewKaku:
            uart0.write("NewKaku");
            break;
        case Protocol::FineOffset:
            uart0.write("FineOffset");
            break;
        case Protocol::SC2262_Everflourish:
            uart0.write("AB400D");
            break;
    }
    uart0.write(";");

    // Send id
    uart0.write("ID=");
    send_(message.identifier());
    uart0.write(";");

    // Send values
    auto &values = message.values();
    for(uint8_t i = 0; i < values.size(); ++i)
    {
        sendValue_(values[i]);
    }

    uart0.write("\r\n");
}

bool RfLinkInterface::receive(RfMessage &message)
{
    bool result = false;
    char buffer[32];

    uint8_t length = uart0.readLine(buffer, sizeof(buffer), '\r');

    const char *data = buffer;
    if(length > 7 && compare("10;", data))
    {
        data += 3;

        if(compare("REBOOT;", data))
        {
        }
        else if(compare("PING;", data))
        {
            sendHeader_();
            uart0.write("PONG;\r\n");
        }
        else if(compare("VERSION;", data))
        {
            sendVersion_();
        }
        else if(compare("RFDEBUG=ON;", data))
        {
            // Show rf packets
            // 20;99;RFDEBUG="state";
        }
        else if(compare("RFDEBUG=OFF;", data))
        {
        }
        else if(compare("RFUDEBUG=ON;", data))
        {
            // Show undecoded rf packets
            // 20;99;RFUDEBUG="state";
        }
        else if(compare("RFUDEBUG=OFF;", data))
        {
        }
        else if(compare("QRFDEBUG=ON;", data))
        {
            // Fast version of rfdebug. Hexadecimal non scaled values.
            // 20;99;QRFDEBUG="state";
        }
        else if(compare("QRFDEBUG=OFF;", data))
        {
        }
        else if(compare("TRISTATEINVERT;", data))
        {
        }
        else if(compare("RTSCLEAN;", data))
        {
        }
        else if(compare("RTSRECCLEAN=", data))
        {
        }
        else if(compare("RTSSHOW;", data))
        {
        }
        else if(compare("RTSINVERT;", data))
        {
        }
        else if(compare("RTSLONGTX;", data))
        {
        }
        else
        {
            // Transmit request
            StringTokenizer tokenizer(data, length - 3, ';');
            uint8_t i = 0;
            while(tokenizer.next())
            {
                auto stringView = tokenizer.token();

                switch(i)
                {
                    case 0: // Protocol
                    {
                        message.protocol(strToProtocol_(stringView.str()));
                        break;
                    }
                    case 1: // Id
                    {
                        uint32_t id = stringToInteger<uint32_t>(
                            stringView.str(), stringView.length(), 16);
                        message.identifier(id);
                        break;
                    }
                    case 2: // Unit (Switch)
                    {
                        uint32_t unit = stringToInteger<uint32_t>(
                            stringView.str(), stringView.length(), 16);
                        message.addValue(ValueType::Unit, unit);
                        break;
                    }
                    case 3: // Command
                    {
                        Command command = Command::Unknown;
                        bool group = false;

                        if(compare("ON", stringView.str()))
                        {
                            command = Command::On;
                        }
                        else if(compare("OFF", stringView.str()))
                        {
                            command = Command::Off;
                        }
                        else if(compare("ALLON", stringView.str()))
                        {
                            command = Command::On;
                            group = true;
                        }
                        else if(compare("ALLOFF", stringView.str()))
                        {
                            command = Command::Off;
                            group = true;
                        }
                        else if(compare("UP", stringView.str()))
                        {
                            command = Command::Up;
                        }
                        else if(compare("DOWN", stringView.str()))
                        {
                            command = Command::Down;
                        }
                        else if(compare("STOP", stringView.str()))
                        {
                            command = Command::Stop;
                        }
                        else if(compare("PAIR", stringView.str()))
                        {
                            command = Command::Pair;
                        }
                        else
                        {
                            command = Command::Unknown;
                        }

                        message.addValue(ValueType::Command,
                                         static_cast<uint32_t>(command));
                        message.addValue(ValueType::Group, group);
                        break;
                    }
                }

                ++i;
            }

            result = true;
        }
    }
    else if(length > 3 && compare("11;", data))
    {
        // Echo
        //        uart0.write(data, length - 3);
        //        uart0.write("\r\n");
    }

    return result;
}

bool RfLinkInterface::compare(const char *str0, const char *str1)
{
    return strcasecmp(str0, str1) <= 0;
}

void RfLinkInterface::sendHeader_()
{
    uart0.write("20;");
    send_(nextIndex_++, 16);
    uart0.write(";");
}

void RfLinkInterface::sendVersion_()
{
    sendHeader_();
    uart0.write("HaLink - HaLink V0.1 - R1;\r\n");
}

void RfLinkInterface::sendValue_(const RfMessage::Value &value)
{
    switch(value.type())
    {
        case ValueType::Id:
            uart0.write("id=");
            send_(value.value(), 16);
            break;
        case ValueType::Group:
            uart0.write("group=");
            send_(value.value(), 10);
            break;
        case ValueType::Unit:
            uart0.write("SWITCH=");
            send_(value.value(), 16);
            break;
        case ValueType::Temperature:
            uart0.write("TEMP=");
            send_(value.value(), 10);
            break;
        case ValueType::RelativeHumidity:
            uart0.write("HUM=");
            send_(value.value(), 10);
            break;
        case ValueType::Command:
            uart0.write("CMD=");
            if(value.value() == 0)
            {
                uart0.write("OFF");
            }
            else if(value.value() == 1)
            {
                uart0.write("ON");
            }
            else if(value.value() == 2)
            {
                uart0.write("X");
            }
            break;
    }
    uart0.write(';');
}

void RfLinkInterface::send_(int32_t value, int base)
{
    ltoa(value, buffer_, base);
    uart0.write(buffer_);
}

void RfLinkInterface::send_(uint64_t value, int base)
{
    // TODO: Fix so that this method can handle 64 bit integers.
    ultoa(value, buffer_, base);
    uart0.write(buffer_);
}

void RfLinkInterface::send_(uint32_t value, int base)
{
    ultoa(value, buffer_, base);
    uart0.write(buffer_);
}

void RfLinkInterface::send_(uint16_t value, int base)
{
    utoa(value, buffer_, base);
    uart0.write(buffer_);
}

void RfLinkInterface::sendProtocol(Protocol protocol)
{
    switch(protocol)
    {
        case Protocol::NewKaku:
            uart0.write("NewKaku");
            break;
        case Protocol::FineOffset:
            uart0.write("FineOffset");
            break;
        case Protocol::SC2262_Everflourish:
            uart0.write("AB400D");
            break;
    }
    uart0.write(';');
}

Protocol RfLinkInterface::strToProtocol_(const char *str)
{
    if(compare("NewKaku;", str))
    {
        return Protocol::NewKaku;
    }
    else if(compare("FineOffset;", str))
    {
        return Protocol::FineOffset;
    }
    else if(compare("AB400D;", str))
    {
        return Protocol::SC2262_Everflourish;
    }
    else
    {
        return Protocol::Unknown;
    }
}
