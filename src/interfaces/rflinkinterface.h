#ifndef RFLINKINTERFACE_H
#define RFLINKINTERFACE_H

#include <inttypes.h>
#include <stdlib.h>

#include "../rfmessage.h"
#include "../rfsignal.h"
#include "../uart.h"

class RfLinkInterface
{
public:
    RfLinkInterface();

    void send(const RfSignal &signal);

    void send(const RfMessage &message);

    bool receive(RfMessage &message);

private:
    bool compare(const char *str0, const char *str1);

    void sendHeader_();
    void sendVersion_();
    void sendValue_(const RfMessage::Value &value);
    void send_(int32_t value, int base = 10);
    void send_(uint64_t value, int base = 10);
    void send_(uint32_t value, int base = 10);
    void send_(uint16_t value, int base = 10);
    void sendProtocol(Protocol protocol);

    Protocol strToProtocol_(const char *str);

    enum class MessageType : uint8_t
    {
        RFLinkToMaster = 20,
        MasterToRfLink = 10,
        Echo = 11
    };

    uint32_t nextIndex_;
    char buffer_[12];
};

#endif // RFLINKINTERFACE_H
