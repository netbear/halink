#include "interfaces/rflinkinterface.h"
#include "pin.h"
#include "radio.h"

#include "protocols/fineoffset.h"
#include "protocols/newkaku.h"
#include "protocols/sc2262.h"

#include <string.h>

using LedPin = Pin<PortB, 7>;

int main()
{
    RfLinkInterface interface;

    radio.init();

    LedPin::asOutput();
    LedPin::clear();

    // Enable interrupts
    sei();

    for(;;)
    {
        if(radio.hasSignal())
        {
            LedPin::set();
            const RfSignal &signal = radio.getSignal();

            RfMessage message;
            bool decoded = false;
            if(NewKakuProtocol::decode(signal, message))
            {
                interface.send(message);
                decoded = true;
            }
            else if(SC2262Protocol::decode(signal, message))
            {
                interface.send(message);
                decoded = true;
            }
            else if(FineOffsetProtocol::decode(signal, message))
            {
                interface.send(message);
                decoded = true;
            }

            if(!decoded && signal.size() >= 16)
            {
                interface.send(signal);
            }

            radio.returnSignal();
            LedPin::clear();
        }

        RfMessage message;
        if(interface.receive(message) &&
           message.protocol() != Protocol::Unknown)
        {
            LedPin::toggle();

            for(int i = 0; i < 4; ++i)
            {
                auto &signal = radio.getTxSignal();

                switch(message.protocol())
                {
                    case Protocol::NewKaku:
                        NewKakuProtocol::encode(message, signal);
                        break;
                    case Protocol::SC2262:
                    case Protocol::SC2262_Everflourish:
                        SC2262Protocol::encode(message, signal);
                        break;
                    default:
                        break;
                }

                radio.returnTxSignal();
            }
        }
    }

    return 0;
}
