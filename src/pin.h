#ifndef PIN_H
#define PIN_H

#include <avr/io.h>
#include <inttypes.h>

#include <avr/iom2560.h>

#define WRAP_REGISTER(reg, name)                                               \
    struct name                                                                \
    {                                                                          \
        static volatile uint8_t &value()                                       \
        {                                                                      \
            return reg;                                                        \
        }                                                                      \
    }

#define WRAP_PORT(port_reg, pin_reg, ddr_reg, name)                            \
    struct name                                                                \
    {                                                                          \
        static volatile uint8_t &port()                                        \
        {                                                                      \
            return port_reg;                                                   \
        }                                                                      \
        static volatile uint8_t &pin()                                         \
        {                                                                      \
            return pin_reg;                                                    \
        }                                                                      \
        static volatile uint8_t &ddr()                                         \
        {                                                                      \
            return ddr_reg;                                                    \
        }                                                                      \
    }

#ifdef PORTA
WRAP_PORT(PORTA, PINA, DDRA, PortA);
#endif
#ifdef PORTB
WRAP_PORT(PORTB, PINB, DDRB, PortB);
#endif
#ifdef PORTC
WRAP_PORT(PORTC, PINC, DDRC, PortC);
#endif
#ifdef PORTD
WRAP_PORT(PORTD, PIND, DDRD, PortD);
#endif
#ifdef PORTE
WRAP_PORT(PORTE, PINE, DDRE, PortE);
#endif
#ifdef PORTF
WRAP_PORT(PORTF, PINF, DDRF, PortF);
#endif
#ifdef PORTG
WRAP_PORT(PORTG, PING, DDRG, PortG);
#endif
#ifdef PORTH
WRAP_PORT(PORTH, PINH, DDRH, PortH);
#endif
#ifdef PORTI
WRAP_PORT(PORTI, PINI, DDRI, PortI);
#endif
#ifdef PORTJ
WRAP_PORT(PORTJ, PINJ, DDRJ, PortJ);
#endif
#ifdef PORTK
WRAP_PORT(PORTK, PINK, DDRK, PortK);
#endif
#ifdef PORTL
WRAP_PORT(PORTL, PINL, DDRL, PortL);
#endif

template <typename Port, uint8_t... all_args> class Pin
{
public:
    static void asOutput()
    {
        Port::ddr() |= mask();
    }

    static void asInput()
    {
        Port::ddr() &= ~mask();
    }

    static void set()
    {
        Port::port() |= mask();
    }

    static void clear()
    {
        Port::port() &= ~mask();
    }

    static void toggle()
    {
        Port::port() ^= mask();
    }

    static uint8_t read()
    {
        return Port::pin() & mask();
    }

private:
#if __cplusplus < 201402L
    static constexpr uint8_t mask_()
    {
        return 0;
    }

    template <typename first_arg_type, typename... next_args_types>
    static constexpr uint8_t mask_(first_arg_type first_arg,
                                   next_args_types... next_args)
    {
        return (1 << first_arg) | mask_(next_args...);
    }

    static constexpr uint8_t mask()
    {
        return mask_(all_args...);
    }
#else
    static constexpr int arg_count = sizeof...(all_args);
    static constexpr int params[arg_count] = {all_args...};

    static constexpr uint8_t mask()
    {
        uint8_t mask = 0;
        for(unsigned int i = 0; i < arg_count; i++)
        {
            mask |= 1 << params[i];
        }
        return mask;
    }
#endif
};

#endif
