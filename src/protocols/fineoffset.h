#ifndef FINEOFFSET_H
#define FINEOFFSET_H

#include "../rfmessage.h"
#include "../rfsignal.h"
#include "../utils.h"

/*
 * T = 500us
 *
 * 0: 1T high, 2T low
 * 1: 3T high, 2T low
 *
 * A full frame:
 * - 8 bit preamble (allways 0xff)
 * - 4 bit type (msb allways 0)
 * - 8 bit id XYYY XXXX (see description bellow)
 * - 12 bit temperature in thenth of a degree, msb is the sign bit
 * - 8 bit humidity en percent
 * - 8 bit crc-8 of the data (4 temperature and humidity)
 *
 * XXXXX = Number displayed on the display when batteries are inserted
 * YYY = Dip switch setting (channel)
 */

class FineOffsetProtocol
{
public:
    static bool decode(const RfSignal &signal, RfMessage &message)
    {
        uint8_t position = 0;
        if(isEqual<uint16_t>(signal.peekHead(96), 1000, 1000 * 0.25))
        {
            position = 96;
        }
        else
        {
            return false;
        }

        uint8_t data[6];
        for(int i = 0; i < 6; ++i)
        {
            bool error = false;
            data[i] = readByte(signal, position, error);

            if(error)
            {
                return false;
            }
        }

        const uint8_t preamble = data[0];
        const uint8_t type = data[1] >> 4;
        const uint8_t id = (data[1] << 4) | (data[2] >> 4);
        const uint16_t temperature =
            ((static_cast<uint16_t>(data[2]) & 0x0f) << 8) | data[3];
        const uint8_t humidity = data[4];
        const uint8_t crc = data[5];
        const uint8_t calculatedCrc = crc8(&data[1], 4, 0x31);

        if(preamble != 0xff || crc != calculatedCrc)
        {
            return false;
        }

        message.protocol(Protocol::FineOffset);
        message.identifier(id);
        message.addValue(ValueType::Id, ((id & 0x80) >> 3) | (id & 0x0f));
        message.addValue(ValueType::Unit, (id >> 4) & 0b111);
        message.addValue(ValueType::Temperature, temperature);
        message.addValue(ValueType::RelativeHumidity, humidity);
        return true;
    }

    static uint8_t readByte(const RfSignal &signal, uint8_t &position,
                            bool &error)
    {
        uint8_t byte = 0x00;
        for(int i = 0; i < 8; ++i)
        {
            byte <<= 1;
            uint8_t bit = readBit(signal, position);
            if(bit == 255)
            {
                error = true;
                return byte;
            }
            else
            {
                byte |= bit;
            }
        }

        error = false;
        return byte;
    }

    static uint8_t readBit(const RfSignal &signal, uint8_t &position)
    {
        uint16_t a = signal.peekHead(position--);
        if(isEqual<uint16_t>(a, 1000, 1000 * 0.25))
        {
            uint16_t b = signal.peekHead(position--);
            if(isEqual<uint16_t>(b, 500, 500 * 0.25))
            {
                return 1;
            }
            else if(isEqual<uint16_t>(b, 1500, 1500 * 0.25))
            {
                return 0;
            }
        }
        return 255;
    }
};

#endif // FINEOFFSET_H
