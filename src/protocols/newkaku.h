#ifndef NEWKAKU_PROTOCOL_H
#define NEWKAKU_PROTOCOL_H

#include "../rfmessage.h"
#include "../rfsignal.h"
#include "../utils.h"

/*
 * T = 250us
 *
 * sync pulse (S): 1T high, 10T low
 * 0: 1T high, 1T low, 1T high, 5T low
 * 1: 1T high, 5T low, 1T high, 1T low
 * X: 1T high, 1T low, 1T high, 1T low
 * pause pulse: 1T high, 40T low
 *
 * A full frame:
 * - Sync Pulse
 * - 26 bit address
 * - 1 bit group
 * - 1 bit on(1)/off(0)/dim(X)
 * - 4 bit unit
 * - [4 bit dim level, only if dim]
 * - Pause Pulse
 */

#include "../uart.h"

class NewKakuProtocol
{
    constexpr static uint16_t t = 250;
    constexpr static uint16_t tShort = 1 * t;
    constexpr static uint16_t tLong = 5 * t;

public:
    static bool decode(const RfSignal &signal, RfMessage &message)
    {
        // Verify the sync pulse pos 131-130 or 135-134 if dim.
        // Ignore the first short pulse since the gain of the receiver has not
        // stabilized.
        uint8_t position = 129;
        if(isEqual<uint16_t>(signal.peekHead(130), 2500, 2500 * 0.25))
        {
            position = 129;
        }
        else if(isEqual<uint16_t>(signal.peekHead(134), 2500, 2500 * 0.25))
        {
            position = 133;
            // TODO: Handle dim. No device to test with.
        }
        else
        {
            // Fail fast, probably not a new kaku signal.
            return false;
        }

        // Address
        uint32_t address = 0;
        for(int i = 0; i < 26; ++i)
        {
            int8_t bit = readBit(signal, position);
            if(bit == -1)
            {
                return false;
            }
            address = address << 1 | bit;
        }

        uint8_t group = readBit(signal, position);
        if(group == -1)
        {
            return false;
        }

        uint8_t cmd = readBit(signal, position);
        if(cmd == -1)
        {
            return false;
        }

        // Unit
        uint8_t unit = 0;
        for(int i = 0; i < 4; ++i)
        {
            int8_t bit = readBit(signal, position);
            if(bit == -1)
            {
                return false;
            }
            unit = unit << 1 | bit;
        }

        // Signal decoded correctly
        message.protocol(Protocol::NewKaku);
        message.identifier(address);
        message.addValue(ValueType::Id, address);
        message.addValue(ValueType::Unit, unit);
        message.addValue(ValueType::Command, cmd);
        message.addValue(ValueType::Group, group);

        return true;
    }

    static bool encode(const RfMessage &message, RfSignal &signal)
    {
        signal.clear();

        // Sync
        signal.push(t);
        signal.push(t * 10);

        RfMessage::Value::Type value;

        // Address
        auto id = message.identifier();
        for(uint32_t m = ((uint32_t)1 << (26 - 1)); m > 0; m >>= 1)
        {
            uint8_t symbol = ((id & m) == 0) ? 0 : 1;
            writeBit(symbol, signal);
        }

        // Group bit
        if(!message.value(ValueType::Group, value))
        {
            return false;
        }
        writeBit(static_cast<int8_t>(value), signal);

        // State bit
        if(!message.value(ValueType::Command, value))
        {
            return false;
        }
        writeBit(static_cast<int8_t>(value), signal);

        // Unit
        if(!message.value(ValueType::Unit, value))
        {
            return false;
        }

        for(uint32_t m = ((uint32_t)1 << (4 - 1)); m > 0; m >>= 1)
        {
            uint8_t symbol = ((value & m) == 0) ? 0 : 1;
            writeBit(symbol, signal);
        }

        // Dim level.

        // Pause
        signal.push(t);
        signal.push(t * 40);
    }

private:
    static void writeBit(uint8_t bit, RfSignal &signal)
    {
        switch(bit)
        {
            case 0:
                signal.push(tShort);
                signal.push(tShort);
                signal.push(tShort);
                signal.push(tLong);
                break;
            case 1:
                signal.push(tShort);
                signal.push(tLong);
                signal.push(tShort);
                signal.push(tShort);
                break;
            case 2:
                signal.push(tShort);
                signal.push(tShort);
                signal.push(tShort);
                signal.push(tShort);
                break;
        }
    }

    static int8_t readBit(const RfSignal &signal, uint8_t &position)
    {
        uint8_t pulses[4];
        for(int i = 0; i < 4; ++i)
        {
            pulses[i] = classifyPulse(signal.peekHead(position--));
        }

        if(pulses[0] == 1 && pulses[1] == 1 && pulses[2] == 1 && pulses[3] == 5)
        {
            return 0;
        }
        else if(pulses[0] == 1 && pulses[1] == 5 && pulses[2] == 1 &&
                pulses[3] == 1)
        {
            return 1;
        }
        else if(pulses[0] == 1 && pulses[1] == 1 && pulses[2] == 1 &&
                pulses[3] == 1)
        {
            return 2;
        }
        return -1;
    }

    static uint8_t classifyPulse(uint16_t duration)
    {
        if(isEqual<uint16_t>(duration, 250, 250 * 0.25))
        {
            return 1;
        }
        else if(isEqual<uint16_t>(duration, 250 * 5, 250 * 5 * 0.25))
        {
            return 5;
        }
        return 0;
    }
};

#endif
