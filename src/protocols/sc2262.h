#ifndef EVERFLOURISH_PROTOCOL_H
#define EVERFLOURISH_PROTOCOL_H

#include "../rfmessage.h"
#include "../rfsignal.h"
#include "../utils.h"
#include <stdlib.h>

/*
 * SC2262/PT2262 (receiver chip  SC2272/PT2272)
 * T = 320us
 *
 * 0: 1T high, 3T, 1T, 3T
 * 1: 3T high, 1T, 3T, 1T
 * 2: 1T high, 3T, 3T, 1T (sc2262 datasheet refers to this bit as f (floating))
 * pause pulse: 1T high, 31T
 *
 * A full frame:
 * - 12 trits address/data (tri state 0/1/2)
 * - Pause Pulse
 *
 * This chip is probably used by many different products.
 *
 * Everflourish (remote with 3 buttons)
 * ABCD = Channel/House (0/2)
 * 123 = Button (0/2)
 * ???? = Unknown, perhaps used for remotes with more than 3 buttons?
 * S = On/Off (1/0)
 *
 *       ABCD 123 ???? S
 * B1ON  2022 022 2222 1
 * B1OFF 2022 022 2222 0
 * B2ON  2022 202 2222 1
 * B2OFF 2022 202 2222 0
 * B3ON  2022 220 2222 1
 * B3OFF 2022 220 2222 0
 *
 * A1ON  0222 022 2222 1
 * B1ON  2022 022 2222 1
 * C1ON  2202 022 2222 1
 * D1ON  2220 022 2222 1
 *       ------------- -
 *       Id (11-trits)  Data (1 trit)
 */

#include "../uart.h"

class SC2262Protocol
{
    static constexpr uint16_t t = 320;

public:
    static bool decode(const RfSignal &signal, RfMessage &message)
    {
        if(!isShort(signal.peekHead(49)) || !isShort(signal.peekHead(1)))
        {
            return false;
        }

        uint8_t position = 49;

        int8_t data[12];
        for(int i = 0; i < 12; ++i)
        {
            int8_t bit = readBit(signal, position);
            if(bit == -1)
            {
                return false;
            }
            data[i] = bit;
        }

        if(decodeEverflourish(data, message))
        {
            return true;
        }
        else
        {
            // Fallback to raw sc2262 data.
            message.protocol(Protocol::SC2262);
            message.identifier(toInteger<uint32_t>(data, 12, 3));
        }

        return true;
    }

    static bool encode(const RfMessage &message, RfSignal &signal)
    {
        int8_t data[12];

        switch(message.protocol())
        {
            case Protocol::SC2262_Everflourish:
                encodeEverflourish(message, data);
                break;
            case Protocol::SC2262:
                // TODO: Implement
                return false;
            default:
                return false;
        }

        signal.clear();

        // 12 address/data trits.
        for(uint8_t i = 0; i < 12; ++i)
        {
            writeBit(data[i], signal);
        }

        // Pause pulse
        signal.push(t);
        signal.push(31 * t);
    }

private:
    static void writeBit(uint8_t bit, RfSignal &signal)
    {
        switch(bit)
        {
            case 0:
                signal.push(t);
                signal.push(3 * t);
                signal.push(t);
                signal.push(3 * t);
                break;
            case 1:
                signal.push(3 * t);
                signal.push(t);
                signal.push(3 * t);
                signal.push(t);
                break;
            case 2:
                signal.push(t);
                signal.push(3 * t);
                signal.push(3 * t);
                signal.push(t);
                break;
        }
    }

    static bool encodeEverflourish(const RfMessage &message, int8_t *data)
    {
        for(uint8_t i = 0; i < 4; ++i)
        {
            if(message.identifier() == i)
            {
                data[i] = 0;
            }
            else
            {
                data[i] = 2;
            }
        }

        int32_t value;
        message.value(ValueType::Unit, value);
        for(uint8_t i = 0; i < 7; ++i)
        {
            if(value == i)
            {
                data[4 + i] = 0;
            }
            else
            {
                data[4 + i] = 2;
            }
        }

        message.value(ValueType::Command, value);
        Command command = static_cast<Command>(value);
        switch(command)
        {
            case Command::Off:
                data[11] = 0;
                break;
            case Command::On:
                data[11] = 1;
                break;
        }
    }

    static bool decodeEverflourish(const int8_t *data, RfMessage &message)
    {
        // Everfluorish
        uint8_t systemCode = 0;
        for(uint8_t i = 0; i < 4; ++i)
        {
            if(data[i] == 1)
            {
                return false;
            }
            else if(data[i] == 0)
            {
                if(systemCode)
                {
                    return false;
                }
                systemCode = i;
            }
        }

        uint8_t unit = 0;
        for(uint8_t i = 0; i < 7; ++i)
        {
            if(data[4 + i] == 1)
            {
                return false;
            }
            else if(data[4 + i] == 0)
            {
                if(unit)
                {
                    return false;
                }
                unit = i;
            }
        }

        if(data[11] == 2)
        {
            return false;
        }
        uint8_t state = data[11];

        message.protocol(Protocol::SC2262_Everflourish);
        message.identifier(systemCode);
        message.addValue(ValueType::Id, systemCode);
        message.addValue(ValueType::Unit, unit);
        message.addValue(ValueType::Command, state);

        return true;
    }

    static int8_t readBit(const RfSignal &signal, uint8_t &position)
    {
        int8_t pulses[4];
        for(int i = 0; i < 4; ++i)
        {
            pulses[i] = classifyPulse(signal.peekHead(position--));
        }

        if(/*pulses[0] == 1 &&*/ pulses[1] == 3 && pulses[2] == 1 &&
           pulses[3] == 3)
        {
            return 0;
        }
        else if(/*pulses[0] == 3 &&*/ pulses[1] == 1 && pulses[2] == 3 &&
                pulses[3] == 1)
        {
            return 1;
        }
        else if(/*pulses[0] == 1 &&*/ pulses[1] == 3 && pulses[2] == 3 &&
                pulses[3] == 1)
        {
            return 2;
        }
        return -1;
    }

    static int8_t classifyPulse(uint16_t duration)
    {
        if(isShort(duration))
        {
            return 1;
        }
        else if(isLong(duration))
        {
            return 3;
        }
        return -1;
    }

    static bool isShort(uint16_t duration)
    {
        return isInRange<uint16_t>(duration, t - 150, t + 150);
    }

    static bool isLong(uint16_t duration)
    {
        return isInRange<uint16_t>(duration, 3 * t - 150, 3 * t + 150);
    }
};

#endif
