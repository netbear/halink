#include "radio.h"

#include <avr/io.h>
#include <inttypes.h>
#include <stdlib.h>
#include <util/atomic.h>
#include <util/delay.h>

Radio radio;

ISR(INT2_vect)
{
    radio.interrupt_();
}

ISR(TIMER1_COMPA_vect)
{
    radio.sendInterrupt_();
}

Radio::Radio() : t0_(0), signal_(nullptr), txSignal_(nullptr)
{
}

void Radio::init()
{
    // Setup io pins
    EnablePin::asOutput();
    EnablePin::clear();

    TxRxModePin::asOutput();
    TxRxModePin::clear();

    TxPin::asOutput();
    TxPin::clear();

    RxPin::asInput();
    RxPin::set(); // Enable pullups

    // Initialize radio (rx mode)
    initRadio_();

    initTimer();
    initExternalInterupt();
}

bool Radio::hasTxSignal() const
{
    return !txBuffers_.full();
}

RfSignal &Radio::getTxSignal()
{
    return txBuffers_.peekHead(-1);
}

void Radio::returnTxSignal()
{
    // Make the buffer available for reading.
    txBuffers_.push();

    // Change mode this will start the transmission.
    setTxMode_();
}

bool Radio::hasSignal() const
{
    return !buffers_.empty();
}

const RfSignal &Radio::getSignal() const
{
    return buffers_.peek();
}

void Radio::returnSignal()
{
    buffers_.pop();
}

void Radio::initRadio_()
{
    // Power down to rx mode
    ATOMIC_BLOCK(ATOMIC_FORCEON)
    {
        EnablePin::set();
        _delay_us(20);
        TxRxModePin::set();
        _delay_us(200);
        TxRxModePin::clear();
        _delay_us(40);
        EnablePin::clear();
        _delay_us(20);
        EnablePin::set();

        txMode_ = false;
    }
}

void Radio::setRxMode_()
{
    // Tx to rx mode.
    ATOMIC_BLOCK(ATOMIC_FORCEON)
    {
        if(txMode_)
        {
            // Set rx mode
            TxRxModePin::clear();
            _delay_us(40);
            EnablePin::clear();
            _delay_us(20);
            EnablePin::set();

            TIMSK1 &= ~(1 << OCIE1A); // Turn off overflow compare interrupt.
            TIFR1 |= (1 << OCF1A);    // Clear queued interrupt.

            txMode_ = false;
        }
    }
}

void Radio::setTxMode_()
{
    // Rx to tx mode.
    ATOMIC_BLOCK(ATOMIC_FORCEON)
    {
        if(!txMode_)
        {
            // Set tx mode
            TxRxModePin::set();

            // It takes 400us for the module to switch into transmission mode.
            OCR1A = TCNT1 + UsToTicks(400);
            TIFR1 |= (1 << OCF1A);   // Clear queued interrupt.
            TIMSK1 |= (1 << OCIE1A); // Enable overflow compare interrupt.

            txMode_ = true;
        }
    }
}

void Radio::initTimer()
{
    TCCR1A = 0b0;
    TCCR1B = 0b11 << CS10;
}

void Radio::initExternalInterupt()
{
    // Set interrupt trigger
    EICRA &= ~(0b11 << ISC20); // Clear bits
    EICRA |= 0b01 << ISC20;    // Set interrupt on both edges

    // Enable interrupt 2
    EIMSK |= 0b1 << INT2;
}

inline void Radio::interrupt_()
{
    const uint16_t t1 = TCNT1;

    // Make sure we have a buffer available
    if(!signal_)
    {
        if(buffers_.full())
        {
            // No buffer available
            return;
        }
        else
        {
            signal_ = &buffers_.peekHead(-1);
            signal_->clear();
        }
    }

    const uint16_t delta = t1 - t0_;
    t0_ = t1;

    if(delta < UsToTicks(100))
    {
        signal_->clear();
        return;
    }

    signal_->push(TicksToUs(delta));

    if(delta > UsToTicks(4500))
    {
        // This is probably a pause pulse
        // Switch buffer
        buffers_.push();
        signal_ = nullptr;
    }
}

void Radio::sendInterrupt_()
{
    if(!txSignal_)
    {
        if(txBuffers_.empty())
        {
            // Disable transmission if it happens to be on.
            TxPin::clear();

            // Nothing more to send at the moment.
            setRxMode_();
            return;
        }
        else
        {
            // Get next buffer;
            txSignal_ = &txBuffers_.peek();
        }
    }

    // Toggle transmission
    TxPin::toggle();

    // Setup delay for next level change.
    const uint16_t t0 = TCNT1;
    const uint16_t delta = txSignal_->pop();
    OCR1A = t0 + UsToTicks(delta);

    // Done with this buffer.
    if(txSignal_->empty())
    {
        txSignal_ = nullptr;
        txBuffers_.pop();
    }
}
