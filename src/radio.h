#ifndef RADIO_H
#define RADIO_H

#include <avr/interrupt.h>
#include <inttypes.h>

#include "pin.h"
#include "rfsignal.h"

ISR(INT2_vect);
ISR(TIMER1_COMPA_vect);

class Radio
{
public:
    Radio();
    void init();

    bool hasTxSignal() const;
    RfSignal &getTxSignal();
    void returnTxSignal();

    bool hasSignal() const;
    const RfSignal &getSignal() const;
    void returnSignal();

private:
    using EnablePin = Pin<PortA, 0>;
    using TxRxModePin = Pin<PortJ, 0>;
    using TxPin = Pin<PortJ, 1>;
    using RxPin = Pin<PortD, 2>;

    void initRadio_();
    void setRxMode_();
    void setTxMode_();

    void initTimer();

    void initExternalInterupt();
    inline void interrupt_() __attribute__((always_inline));
    friend void INT2_vect();

    inline void sendInterrupt_() __attribute__((always_inline));
    friend void TIMER1_COMPA_vect();

    static constexpr uint16_t UsToTicks(const uint16_t t)
    {
        return t >> 2; // Divide by 4
    }

    static constexpr uint16_t TicksToUs(const uint16_t t)
    {
        return t << 2; // Multiply by 4
    }

    volatile bool txMode_;

    uint16_t t0_;
    CircularBuffer<RfSignal, 3, false> buffers_;
    RfSignal *signal_;

    CircularBuffer<RfSignal, 3, false> txBuffers_;
    RfSignal *txSignal_;
};

extern Radio radio;

#endif
