#ifndef RFMESSAGE_H
#define RFMESSAGE_H

#include "vector.h"
#include <inttypes.h>
#include <string.h>

enum class ValueType : uint16_t
{
    Id, // Unique id or address of the device.

    Unit, // Function of device. e.g. a remote might have multiple button
          // groups (on/off), each button group would be one unit.

    Group, // One if this is a group action. Some remotes has buttons to control
           // more than one device.

    Command, // On/Off/..., see Command enum

    Temperature, // Temperature in degree celcius.

    RelativeHumidity // Relative humidity in percent.
};

enum class Protocol : uint16_t
{
    Unknown,
    SC2262,              // SC2262/PT2262 (receiver chip SC2272/PT2272)
    SC2262_Everflourish, // Uses SC2262
    FineOffset,
    NewKaku
};

enum class Command : uint8_t
{
    Off,
    On,
    Up,
    Down,
    Stop,
    Pair,
    Unknown
};

template <typename T> class RfValue
{
public:
    using Type = T;

    RfValue()
    {
    }

    RfValue(ValueType type, T value) : type_(type), value_(value)
    {
    }

    ValueType type() const
    {
        return type_;
    }

    const T &value() const
    {
        return value_;
    }

private:
    ValueType type_;
    T value_;
};

class RfMessage
{
public:
    using Value = RfValue<int32_t>;
    using ValueVector = Vector<Value, 8>;

    RfMessage() : protocol_(Protocol::Unknown)
    {
    }

    bool operator==(const RfMessage &m)
    {
        return protocol_ == m.protocol_ && id_ == m.id_ &&
               values_.size() == m.values_.size();
    }

    bool operator!=(const RfMessage &m)
    {
        return !(*this == m);
    }

    void protocol(Protocol protocol)
    {
        protocol_ = protocol;
    }

    Protocol protocol() const
    {
        return protocol_;
    }

    void identifier(uint64_t id)
    {
        id_ = id;
    }

    uint64_t identifier() const
    {
        return id_;
    }

    bool addValue(ValueType type, Value::Type value)
    {
        if(values_.full())
        {
            return false;
        }
        values_.push_back(Value(type, value));
        return true;
    }

    const ValueVector &values() const
    {
        return values_;
    }

    const bool value(ValueType type, Value::Type &value) const
    {
        for(uint8_t i = 0; i < values_.size(); ++i)
        {
            if(values_[i].type() == type)
            {
                value = values_[i].value();
                return true;
            }
        }
        return false;
    }

private:
    Protocol protocol_;
    uint64_t id_;
    ValueVector values_;
};

#endif
