#ifndef RFSIGNAL
#define RFSIGNAL

#include "circularbuffer.h"
#include <inttypes.h>

using RfSignal = CircularBuffer<volatile uint16_t, 256>;

#endif
