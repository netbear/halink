#include "stringtokenizer.h"

StringTokenizer::StringTokenizer(const char *string, uint8_t length,
                                 char delimiter)
    : string_(string), length_(length), delimiter_(delimiter), tokenStart_(0),
      tokenEnd_(0)
{
}

bool StringTokenizer::next()
{
    bool hasNext = false;

    tokenStart_ = tokenEnd_;

    for(uint8_t i = tokenStart_; i < length_; ++i)
    {
        if(string_[i] == delimiter_)
        {
            hasNext = true;
            tokenEnd_ = i + 1;
            break;
        }
    }
    return hasNext;
}

StringView StringTokenizer::token() const
{
    return StringView(string_ + tokenStart_, (tokenEnd_ - tokenStart_) - 1);
}
