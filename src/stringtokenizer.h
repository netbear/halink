#ifndef STRINGTOKENIZER_H
#define STRINGTOKENIZER_H

#include "stringview.h"
#include <inttypes.h>

class StringTokenizer
{
public:
    StringTokenizer(const char *string, uint8_t length, char delimiter);

    bool next();
    StringView token() const;

private:
    const char *string_;
    uint8_t length_;
    char delimiter_;

    uint8_t tokenStart_;
    uint8_t tokenEnd_;
};

#endif // STRINGTOKENIZER_H
