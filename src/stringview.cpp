#include "stringview.h"

StringView::StringView(const char *string, uint8_t length)
    : str_(string), length_(length)
{
}

uint8_t StringView::length() const
{
    return length_;
}

const char *StringView::str() const
{
    return str_;
}
