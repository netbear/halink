#ifndef STRINGVIEW_H
#define STRINGVIEW_H

#include <inttypes.h>

class StringView
{
public:
    StringView(const char *string, uint8_t length);

    uint8_t length() const;
    const char *str() const;

private:
    const char *str_;
    uint8_t length_;
};

#endif // STRINGVIEW_H
