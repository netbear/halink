#include "time.h"

#include <avr/io.h>

Time time;

ISR(TIMER0_OVF_vect)
{
    time.overflowCount_++;
}

Time::Time()
{
    // TIMSK0 |= 1 << TOIE0;

    // TCCR0A = 0b11 << WGM00; // Fast PWM
    // TCCR0B = 0b011 << CS00; // clk/64   
}

unsigned long Time::us()
{
    // Disable interrupts
    uint8_t oldSREG = SREG;
    cli();

    unsigned long m = overflowCount_;
    uint8_t t = TCNT0;

    if ((TIFR0 & _BV(TOV0)) && (t < 255)) {
        m++;
    }    

    // Restore
    SREG = oldSREG;
    return ((m << 8) + t) * (64 / CLOCK_CYCLES_PER_US);
}