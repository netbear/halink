#ifndef TIME_H
#define TIME_H

#include <inttypes.h>
#include <avr/interrupt.h>

ISR(TIMER0_OVF_vect);

class Time
{
public:
    Time();
    unsigned long us();

private:
    void interruptHandler();
    volatile unsigned long overflowCount_;

    static constexpr unsigned long CLOCK_CYCLES_PER_US = F_CPU / 1e6;

    friend void TIMER0_OVF_vect(void);
};

extern Time time;

#endif