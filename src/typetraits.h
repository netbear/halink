#ifndef TYPETRAITS_H
#define TYPETRAITS_H

#include <inttypes.h>

template <uint64_t N> struct constant
{
    enum
    {
        value = N
    };
};

template <typename T> struct return_
{
    typedef T type;
};

// Bit count
template <uint64_t N> struct bitcount : constant<1 + bitcount<(N >> 1)>::value>
{
};

template <> struct bitcount<0> : constant<1>
{
};

template <> struct bitcount<1> : constant<1>
{
};

// Byte count
template <uint64_t N>
struct bytecount : constant<((bitcount<N>::value + 7) >> 3)>
{
};

// Byte type
template <uint64_t N> struct bytetype : return_<uint64_t>
{
};

template <> struct bytetype<4> : return_<uint32_t>
{
};

template <> struct bytetype<3> : return_<uint32_t>
{
};

template <> struct bytetype<2> : return_<uint16_t>
{
};

template <> struct bytetype<1> : return_<uint8_t>
{
};

// Integer type
template <uint64_t N> struct integer_type : bytetype<bytecount<N>::value>
{
};

// Is same
template <typename T0, typename T1> struct is_same : constant<0>
{
};

template <typename T> struct is_same<T, T> : constant<1>
{
};

#endif // TYPETRAITS_H
