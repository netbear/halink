#include "uart.h"

ISR(USART0_RX_vect)
{
    uart0.rxInterruptHandler();
}

ISR(USART0_UDRE_vect)
{
    uart0.udreInterruptHandler();
}

Uart<64, 64> uart0;
