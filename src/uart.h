#ifndef UART_H
#define UART_H

#include <avr/interrupt.h>
#include <avr/io.h>
#include <inttypes.h>

#include "circularbuffer.h"

constexpr uint16_t uartBaudSelect(unsigned long baudRate,
                                  bool doubleSpeed = false)
{
    if(doubleSpeed)
    {
        return ((((F_CPU) + 4UL * (baudRate)) / (8UL * (baudRate)) - 1) |
                0x8000);
    }
    else
    {
        return (((F_CPU) + 8UL * (baudRate)) / (16UL * (baudRate)) - 1UL);
    }
}

ISR(USART0_RX_vect);
ISR(USART0_UDRE_vect);

template <uint16_t txBufferSize, uint16_t rxBufferSize> class Uart
{
public:
    void init(uint16_t ubrr)
    {
        if(ubrr & 0x8000)
        {
            UBRR0 = ubrr & ~0x8000;
            UCSR0A |= (1 << U2X0);
        }
        else
        {
            UBRR0 = ubrr;
            UCSR0A &= ~(1 << U2X0);
        }

        UCSR0B = (1 << RXCIE0) | (0 << TXCIE0) | (1 << UDRIE0) | (1 << RXEN0) |
                 (1 << TXEN0) | (0 << UCSZ02);
        UCSR0C = (0b11 << UCSZ00);
    }

    void write(uint8_t c)
    {
        while(!txBuffer_.push(c))
            ;

        // Enable interrupt
        UCSR0B |= (1 << UDRIE0);
    }

    void write(uint8_t *data, uint8_t length)
    {
        for(int i = 0; i < length; ++i)
        {
            write(data[i]);
        }
    }

    void write(char *str)
    {
        while(*str != '\0')
        {
            write(*str);
            ++str;
        }
    }

    bool read(uint8_t &c)
    {
        c = rxBuffer_.pop();
        return true;
    }

    bool read(char *data, uint8_t length)
    {
        for(uint8_t i = 0; i < length; ++i)
        {
            data[i] = rxBuffer_.pop();
        }
        return true;
    }

    uint8_t readLine(char *data, uint8_t length, char delimiter = '\n')
    {
        uint8_t n = 0;
        for(uint8_t i = 0; i < rxBuffer_.size(); ++i)
        {
            if(rxBuffer_.peek(i) == delimiter)
            {
                n = i + 1;
                read(data, n);

                break;
            }
        }
        return n;
    }

    uint16_t available()
    {
        return rxBuffer_.size();
    }

private:
    void rxInterruptHandler()
    {
        rxBuffer_.push(UDR0);
    }

    void udreInterruptHandler()
    {
        if(!txBuffer_.empty())
        {
            UDR0 = txBuffer_.pop();
        }
        else
        {
            // Disable interrupt
            UCSR0B &= ~(1 << UDRIE0);
        }
    }

    friend void USART0_RX_vect();
    friend void USART0_TX_vect();
    friend void USART0_UDRE_vect();

    CircularBuffer<volatile uint8_t, txBufferSize, false> txBuffer_;
    CircularBuffer<volatile uint8_t, rxBufferSize, false> rxBuffer_;
};

extern Uart<64, 64> uart0;

#endif // UART_H
