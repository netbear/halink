#include "utils.h"

uint8_t crc8(const uint8_t *data, uint8_t length, uint8_t polynomial)
{
    uint8_t crc = 0;
    for(int i = 0; i < length; ++i)
    {
        crc = crc8(crc, data[i], polynomial);
    }
    return crc;
}

uint8_t crc8(uint8_t crc, const uint8_t data, const uint8_t polynomial)
{
    for(uint8_t mask = 0x80; mask > 0; mask >>= 1)
    {
        uint8_t bit = crc & 0x80;
        if(data & mask)
        {
            bit = !bit;
        }

        crc <<= 1;

        if(bit)
        {
            crc ^= polynomial;
        }
    }
    return crc;
}
