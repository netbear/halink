#ifndef UTILS_H
#define UTILS_H

#include <inttypes.h>
#include <math.h>
#include <string.h>

template <class T> T diff(T a, T b)
{
    return a > b ? a - b : b - a;
}

template <class T> T isEqual(T a, T b, T tolerance)
{
    return diff(a, b) <= tolerance;
}

template <class T> T isInRange(T a, T lowerLimit, T upperLimit)
{
    return a >= lowerLimit && a <= upperLimit;
}

uint8_t crc8(uint8_t crc, const uint8_t data, const uint8_t polynomial);
uint8_t crc8(const uint8_t *data, uint8_t length, uint8_t polynomial);

template <typename Integer, typename Symbol>
constexpr Integer symbolMap(Symbol symbol)
{
    if(symbol >= '0' && symbol <= '9')
    {
        return symbol - '0';
    }
    else if(symbol >= 'a' && symbol <= 'z')
    {
        return 10 + (symbol - 'a');
    }
    else if(symbol >= 'A' && symbol <= 'Z')
    {
        return 10 + (symbol - 'A');
    }
    else
    {
        return symbol;
    }
}

template <typename Integer, typename Data>
Integer stringToInteger(const Data *data, uint8_t length, uint8_t base)
{
    Integer result = 0;
    uint32_t power = 1;
    for(int8_t i = length - 1; i >= 0; --i)
    {
        result += symbolMap<Integer>(data[i]) * power;
        power = power * base;
    }
    return result;
}

/**
 * @brief Convert number stored in data from given base to decimal.
 *
 * The data array is not a string, to convert a ternary number the
 * data array should contain the symbolse 0, 1 and 2. For a hexadecimal
 * number the symbols 0,1,2,...,9,10,11,...,15 should be used.
 *
 * @param data Array of symbols.
 * @param length Length of the array.
 * @param base Base to convert from.
 */
template <typename Integer, typename Data>
Integer toInteger(const Data *data, uint8_t length, uint8_t base)
{
    Integer result = 0;
    uint32_t power = 1;
    for(int8_t i = length - 1; i >= 0; --i)
    {
        result += data[i] * power;
        power = power * base;
    }
    return result;
}

#endif
