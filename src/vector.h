#ifndef VECTOR_H
#define VECTOR_H

#include "typetraits.h"

template <class T, int CAPACITY> class Vector
{
public:
    typedef T value_t;
    typedef typename integer_type<CAPACITY>::type size_t;

    Vector() : size_(0)
    {
    }

    value_t &operator[](size_t index)
    {
        return data_[index];
    }

    const value_t &operator[](size_t index) const
    {
        return data_[index];
    }

    void push_back(const value_t &value)
    {
        data_[size_++] = value;
    }

    void pop_back()
    {
        --size_;
    }

    size_t size() const
    {
        return size_;
    }

    size_t capacity() const
    {
        return CAPACITY;
    }

    bool full() const
    {
        return size_ == CAPACITY;
    }

    bool empty() const
    {
        return size_ == 0;
    }

private:
    size_t size_;
    value_t data_[CAPACITY];
};

#endif // VECTOR_H
